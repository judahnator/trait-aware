v2.0.0
======

 * PHP 7.4 is now required as te minimum version.
 * Moving the `class_uses_deep` function to `judahntor\TraitAware\class_uses_deep`.
 * Adding code coverage to tests.

v1.0.1
======
Adding tests for PHP 7.3.

v1.0.0
======

Initial release.