<?php

namespace judahnator\TraitAware\Tests;

use judahnator\TraitAware\Tests\Helpers\BarClass;
use judahnator\TraitAware\Tests\Helpers\BarTrait;
use judahnator\TraitAware\Tests\Helpers\FooTrait;
use judahnator\TraitAware\Tests\Helpers\FooClass;
use judahnator\TraitAware\TraitAware;
use PHPUnit\Framework\TestCase;

final class TraitAwareTest extends TestCase
{
    /**
     * @coversNothing
     */
    public function testTraitExists(): void
    {
        $this->assertTrue(trait_exists(TraitAware::class));
    }

    /**
     * @covers \judahnator\TraitAware\class_uses_deep
     * @covers \judahnator\TraitAware\TraitAware::getTraits
     */
    public function testNormalUsage(): void
    {
        $this->assertSame(array_values(class_uses(FooClass::class)), FooClass::getTraits());
    }

    /**
     * @covers \judahnator\TraitAware\class_uses_deep
     * @covers \judahnator\TraitAware\TraitAware::getTraits
     */
    public function testDeepUsage(): void
    {
        $class_uses = BarClass::getTraits();
        $this->assertTrue(in_array(FooTrait::class, $class_uses));
        $this->assertTrue(in_array(BarTrait::class, $class_uses));
    }
}
