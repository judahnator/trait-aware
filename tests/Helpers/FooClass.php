<?php

namespace judahnator\TraitAware\Tests\Helpers;

use judahnator\TraitAware\TraitAware;

class FooClass
{
    use FooTrait, TraitAware;
}
