<?php

namespace judahnator\TraitAware\Tests;

use function judahnator\TraitAware\class_uses_deep;
use judahnator\TraitAware\Tests\Helpers\BarClass;
use judahnator\TraitAware\Tests\Helpers\BarTrait;
use judahnator\TraitAware\Tests\Helpers\FooTrait;
use judahnator\TraitAware\Tests\Helpers\FooClass;
use PHPUnit\Framework\TestCase;

final class ClassUsesDeepTest extends TestCase
{
    /**
     * @coversNothing
     */
    public function testFunctionExists(): void
    {
        $this->assertTrue(function_exists('judahnator\TraitAware\class_uses_deep'));
    }

    /**
     * @covers \judahnator\TraitAware\class_uses_deep
     */
    public function testNormalUsage(): void
    {
        $this->assertSame(class_uses(FooClass::class), class_uses_deep(FooClass::class));
    }

    /**
     * @covers \judahnator\TraitAware\class_uses_deep
     */
    public function testDeepUsage(): void
    {
        $class_uses = class_uses_deep(BarClass::class);
        $this->assertArrayHasKey(FooTrait::class, $class_uses);
        $this->assertTrue(in_array(FooTrait::class, $class_uses));

        $this->assertArrayHasKey(BarTrait::class, $class_uses);
        $this->assertTrue(in_array(BarTrait::class, $class_uses));
    }
}
