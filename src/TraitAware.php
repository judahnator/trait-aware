<?php

namespace judahnator\TraitAware;

trait TraitAware
{
    /**
     * Returns all the traits this class uses recursively.
     *
     * @param bool $autoload
     * @return array<string>
     */
    final public static function getTraits(bool $autoload = true): array
    {
        return array_values(class_uses_deep(static::class, $autoload));
    }
}
